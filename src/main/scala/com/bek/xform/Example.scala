package com.bek.xform

//import org.apache.spark.SparkConf
//import org.apache.spark.SparkContext
//import org.joda.time.DateTime
import java.time._
//import SparkContext._

import kafka.serializer.{Decoder, DefaultDecoder, StringDecoder}
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.dstream.DStream
//import org.apache.kafka.common.serialization.ByteArraySerializer
//import org.apache.kafka.common.serialization.ByteArrayDeserializer

object Example {
  
  def main(args: Array[String]) {
    
    // Create context with 1 second interval
    val sparkConf = new SparkConf().setAppName("StepEventTransformer")
    val ssc = new StreamingContext(sparkConf, Seconds(1))
    
    val kafkaParams = Map(
      "bootstrap.servers" -> "localhost:9092",
      "auto.offset.reset" -> "smallest",
      "group.id" -> "mygroup")
    
    val topicVals = "acmestepevents"
    val topics = topicVals.split(",").toSet
    
    val msgs: InputDStream[(Array[Byte], Array[Byte])] = KafkaUtils.createDirectStream[Array[Byte],Array[Byte],DefaultDecoder, DefaultDecoder](ssc, kafkaParams, topics)
    
    val msgByteArrays: DStream[Array[Byte]] = msgs.map(_._2)
             
    import com.rbh.avro.partner.acme.event.{AcmeStepEvent, Data}
    import org.apache.avro.io.{BinaryDecoder, Decoder, DecoderFactory}
    import java.io.ByteArrayInputStream
    import org.apache.avro.specific.SpecificDatumReader
    
    msgByteArrays.foreachRDD { rdd =>
      rdd.foreachPartition { partition => 
        val empty = new ByteArrayInputStream(Array.emptyByteArray)
        val reuseAcmeStepEvent: AcmeStepEvent = new AcmeStepEvent()
        val decoderFactory = new DecoderFactory()
        val reuseBinaryDecoder: BinaryDecoder = decoderFactory.binaryDecoder(empty, null)
        
        partition.foreach { record: Array[Byte] =>
          val bais: ByteArrayInputStream = new ByteArrayInputStream(record)
          // TODO: Reuse BinaryDecoder (?)
          // The DecoderFactory will re-initialize the BinaryDecoder or create one if null.
          val decoder: Decoder = decoderFactory.binaryDecoder(bais, reuseBinaryDecoder)
          val specificDatumReader = new SpecificDatumReader[AcmeStepEvent](classOf[AcmeStepEvent])
          // Read will re-initialize the POJO or create one if null.
          val acmeStepEvent: AcmeStepEvent = specificDatumReader.read(reuseAcmeStepEvent, decoder)
          
          // So now we can do whatever we would like with this event type:
          //  * Send to message bus
          //  * Write to durable storage
          //  * Log/discard/aggregate
          //  * Return to new topic(s)
          //  * Send along direct to some other endpoint/API
          // For illustration purposes we'll just print out some crap...
          
          val transactionId: Long = acmeStepEvent.getTransactionid
          val partnerId: Long = acmeStepEvent.getPartnerid
          val eventTS: Long = acmeStepEvent.getEventtimestamp
          val receivedTS: Long = acmeStepEvent.getReceivedtimestamp
          val nbrOfSteps: Int = acmeStepEvent.getNbrofsteps
          
          println(s">>> $transactionId with #$nbrOfSteps happened @$eventTS and was received @$receivedTS")
          
        }
      }
    }
    
    // Spark it up.
    ssc.start()
    ssc.awaitTermination()
  }
  
}
