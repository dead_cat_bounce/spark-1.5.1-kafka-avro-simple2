name := "stepper2"

version := "0.1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-streaming-kafka" % "1.5.2",
  "org.apache.spark" %% "spark-streaming" % "1.5.2" % "provided",
  "org.apache.spark" %% "spark-core" % "1.5.2" % "provided",
  "org.apache.kafka" %% "kafka" % "0.8.2.2",
  "joda-time" % "joda-time" % "2.7"
)
  
