# Template
SBT Spark template application.

## Goal
To provide a basic shell of an application for Spark developement in Scala using sbt.

### Other Details
Assemble this into a jar file using:

user@machine:~/spark/template$ sbt
>;clean;compile;assembly

Make note of the location of the resulting jar file.  E.g.

[info] Packaging /home/user/spark/template/target/scala-2.10/template.jar ...

Assuming local spark stand alone, execute using:


user@machine:~/spark/current$ bin/spark-submit --class com.redbrickhealth.example.Example --master local[1] /home/user/spark/template/target/scala-2.10/template.jar

A bunch of output will roll by and then you should see something like:

Template example executed at 2015-05-29T13:41:35.691-05:00

### Other Goals
None.
